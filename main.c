#include "ft_printf.h"
#include <stdio.h>

int main (void) {
    printf("\n-------------------------------------\n");
    ft_printf("FAKE: %%-5.3s LYDI == |%-5.3s|\n", "LYDI");
    printf("REAL: %%-5.3s LYDI == |%-5.3s|\n", "LYDI");
    printf("\n-------------------------------------\n");
    ft_printf("FAKE: %%04.5i 42 == |%04.5i|\n", 42);
    printf("REAL: %%04.5i 42 == |%04.5i|\n", 42);
//    ft_printf("%%");
//    printf("\n%%");
    printf("\n-------------------------------------\n");
    return 0;
}