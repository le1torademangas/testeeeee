/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_type.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: relizabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/24 11:07:15 by relizabe          #+#    #+#             */
/*   Updated: 2021/01/24 11:07:25 by relizabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	check_type(char c)
{
	int i;

	i = 0;
	while (TP[i])
	{
		if (c == TP[i])
			return (1);
		i++;
	}
	return (0);
}

int	check_flags(char c)
{
	int i;

	i = 0;
	while (FLAGS[i])
	{
		if (c == FLAGS[i])
			return (1);
		i++;
	}
	return (0);
}
