/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoibase.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: relizabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/24 11:08:00 by relizabe          #+#    #+#             */
/*   Updated: 2021/01/24 11:08:17 by relizabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_atoibase(char *str)
{
	int i;
	int j;
	int res;

	j = 1;
	res = 0;
	i = ft_strlen(str);
	while (--i >= 0)
	{
		res += j * (str[i] - '0');
		j *= 2;
	}
	return (res);
}
