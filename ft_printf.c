/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: relizabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/24 11:10:54 by relizabe          #+#    #+#             */
/*   Updated: 2021/01/24 12:00:04 by relizabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_printf(const char *fmt, ...)
{
	va_list		args;
	t_list_spec	cr;

	cr.count = 0;
	va_start(args, fmt);
	while (*fmt)
	{
		if (*fmt == '%')
		{
			fmt++;
//			char *str = fmt;
//			int count = 0;
//			int check = 0;
//			while (*str != '%' && !check_type(*str)) {
//				if (!ft_isdigit_char(*str) && *str != '-') {
//					check = 1;
//				}
//				str++;
//				count++;
//			}
			if (*fmt == '%') {
//				fmt += count;
				cr.count += write(1, fmt++, 1);
				continue ;
			}
//			else {
				if (!(main_call(&fmt, &args, &cr)))
					break ;
//			}
		}
		else
			cr.count += write(1, fmt, 1);
		fmt++;
	}
	va_end(args);
	return (cr.count);
}
